FROM node:12

# Create service directory
WORKDIR /usr/src/service

## Add the wait script to the image
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.7.3/wait /wait
RUN chmod +x /wait

# Copy service code into service directory
COPY . .

# Install dependencies
RUN npm install

# Launch de service
CMD /wait && node index.js
