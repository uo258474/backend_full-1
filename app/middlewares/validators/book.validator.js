const {body, param, checkBody, validationResult} = require('express-validator');
const jwt = require('express-jwt');
const auth = require('../../config/auth.config');
const path = require('path');


module.exports.create = [
  jwt({ secret: auth.secret, algorithms: [ auth.algorithm ] }),
  body('title')
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage('Book title cannot be empty!')
    .bail(),
  body('author')
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage('Author cannot be empty!')
    .bail()
    .isLength({min: 3})
    .withMessage('Minimum 3 characters required!')
    .bail(),
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty())
      return res.status(422).json({errors: errors.array()});
    next();
  },
];

module.exports.upload = [
  jwt({ secret: auth.secret, algorithms: [ auth.algorithm ] }),
  param('id', 'missing book id')
    .exists()
    .isNumeric()
    .bail(),
  body('coverFile', 'Please upload cover image file!')
    .custom((value, { req }) => {
      const extension = (path.extname(req.files.coverFile.name)).toLowerCase();
      switch (extension) {
        case '.jpg':
          return '.jpg';
        case '.jpeg':
          return '.jpeg';
        case  '.png':
          return '.png';
        default:
          return false;
      }
    })
    .bail(),
  (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty())
        return res.status(422).json({errors: errors.array()});
      next();
  },
];
